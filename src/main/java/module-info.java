module org.refcodes.net {

	requires org.refcodes.mixin;
	requires org.refcodes.runtime;
	requires org.refcodes.struct;
	requires transitive org.refcodes.exception;

	exports org.refcodes.net;
}
