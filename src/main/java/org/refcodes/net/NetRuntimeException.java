// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.net;

import org.refcodes.exception.AbstractRuntimeException;
import org.refcodes.mixin.PortAccessor;

/**
 * The base runtime exception for networking related issues.
 */
public abstract class NetRuntimeException extends AbstractRuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * {@inheritDoc}
	 */
	public NetRuntimeException( String aMessage, String aErrorCode ) {
		super( aMessage, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public NetRuntimeException( String aMessage, Throwable aCause, String aErrorCode ) {
		super( aMessage, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public NetRuntimeException( String aMessage, Throwable aCause ) {
		super( aMessage, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public NetRuntimeException( String aMessage ) {
		super( aMessage );
	}

	/**
	 * {@inheritDoc}
	 */
	public NetRuntimeException( Throwable aCause, String aErrorCode ) {
		super( aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public NetRuntimeException( Throwable aCause ) {
		super( aCause );
	}

	/**
	 * Base runtime exception for port related problems.
	 */
	protected abstract static class PortRuntimeException extends NetRuntimeException implements PortAccessor {

		private static final long serialVersionUID = 1L;
		protected int _port;

		/**
		 * {@inheritDoc}
		 * 
		 * @param aPort The port in question.
		 */
		public PortRuntimeException( String aMessage, int aPort, String aErrorCode ) {
			super( aMessage, aErrorCode );
			_port = aPort;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aPort The port in question.
		 */
		public PortRuntimeException( String aMessage, int aPort, Throwable aCause, String aErrorCode ) {
			super( aMessage, aCause, aErrorCode );
			_port = aPort;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aPort The port in question.
		 */
		public PortRuntimeException( String aMessage, int aPort, Throwable aCause ) {
			super( aMessage, aCause );
			_port = aPort;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aPort The port in question.
		 */
		public PortRuntimeException( String aMessage, int aPort ) {
			super( aMessage );
			_port = aPort;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aPort The port in question.
		 */
		public PortRuntimeException( int aPort, Throwable aCause, String aErrorCode ) {
			super( aCause, aErrorCode );
			_port = aPort;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aPort The port in question.
		 */
		public PortRuntimeException( int aPort, Throwable aCause ) {
			super( aCause );
			_port = aPort;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public int getPort() {
			return _port;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Object[] getPatternArguments() {
			return new Object[] { _port };
		}
	}
}
