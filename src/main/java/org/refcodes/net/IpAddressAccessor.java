// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.net;

/**
 * Provides an accessor for a IP-Address property. The IP-Address is represented
 * as an array of ints. This interface provides means to convert it from and to
 * its {@link String} representation via {@link #toCidrNotation()} and
 * {@link IpAddressMutator#fromCidrNotation(String)}.
 */
public interface IpAddressAccessor {

	/**
	 * Retrieves the IP-Address from the IP-Address property.
	 * 
	 * @return The IP-Address stored by the IP-Address property.
	 */
	int[] getIpAddress();

	/**
	 * Constructs the IP-Address {@link String} in CIDR notation from the
	 * IP-Address property. The {@link String} is provided as CIDR notation as
	 * of
	 * "https://en.wikipedia.org/wiki/Classless_Inter-Domain_Routing#CIDR_notation".
	 * 
	 * @return The IP-Address converted to a {@link String} by the IP-Address
	 *         property.
	 */
	default String toCidrNotation() {
		final int[] theIpAddress = getIpAddress();
		if ( theIpAddress == null ) {
			return null;
		}
		if ( theIpAddress.length == 0 ) {
			return "";
		}
		try {
			return IpAddress.toString( theIpAddress );
		}
		catch ( IllegalArgumentException e ) {
			throw new IllegalStateException( "Cannot create CIDR notation for IP address <" + theIpAddress + ">!", e );
		}
	}

	/**
	 * Provides a mutator for a IP-Address property.
	 */
	public interface IpAddressMutator {
		/**
		 * Sets the IP-Address for the IP-Address property.
		 * 
		 * @param aIpAddress The IP-Address to be stored by the IP-Address
		 *        property.
		 */
		void setIpAddress( int[] aIpAddress );

		/**
		 * Constructs the IP-Address from the given {@link String} and sets it
		 * for the IP-Address property. The {@link String} must be provided in
		 * CIDR notation as of
		 * "https://en.wikipedia.org/wiki/Classless_Inter-Domain_Routing#CIDR_notation".
		 * 
		 * @param aCidrNotation The IP-Address {@link String} in CIDR notation
		 *        to be converted and stored by the IP-Address property.
		 */
		default void fromCidrNotation( String aCidrNotation ) {
			setIpAddress( IpAddress.fromAnyCidrNotation( aCidrNotation ) );
		}
	}

	/**
	 * Provides a builder method for a IP-Address property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface IpAddressBuilder<B extends IpAddressBuilder<B>> {

		/**
		 * Sets the IP-Address for the IP-Address property.
		 * 
		 * @param aIpAddress The IP-Address to be stored by the IP-Address
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withIpAddress( int[] aIpAddress );

		/**
		 * Constructs the IP-Address from the given {@link String} and sets it
		 * for the IP-Address property. The {@link String} must be provided in
		 * CIDR notation as of
		 * "https://en.wikipedia.org/wiki/Classless_Inter-Domain_Routing#CIDR_notation".
		 * 
		 * @param aCidrNotation The IP-Address {@link String} in CIDR notation
		 *        to be converted and stored by the IP-Address property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withCidrNotation( String aCidrNotation );
	}

	/**
	 * Provides a IP-Address property.
	 */
	public interface IpAddressProperty extends IpAddressAccessor, IpAddressMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given integer array (setter)
		 * as of {@link #setIpAddress(int[])} and returns the very same value
		 * (getter).
		 * 
		 * @param aIpAddress The integer array to set (via
		 *        {@link #setIpAddress(int[])}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default int[] letIpAddress( int[] aIpAddress ) {
			setIpAddress( aIpAddress );
			return aIpAddress;
		}
	}
}
