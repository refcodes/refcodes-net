/*
 * 
 */
package org.refcodes.net;

import org.refcodes.struct.Range;

/**
 * Port range definitions as of RFC 6335 ("https://tools.ietf.org/html/rfc6335")
 */
public enum PortRange implements Range<Integer> {

	// @formatter:off
	SYSTEM_PORTS(0, 1023), 
	
	REGISTERED_PORTS(1024, 49151),
	
	DYNAMIC_PORTS(49152, 65535),
	
	LINUX_DYNAMIC_PORTS(32768, 61000);
	// @formatter:on

	private int _minPort;

	private int _maxPort;

	/**
	 * Instantiates a new port range.
	 *
	 * @param aMinPort the min port
	 * @param aMaxPort the max port
	 */
	private PortRange( int aMinPort, int aMaxPort ) {
		_minPort = aMinPort;
		_maxPort = aMaxPort;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer getMinValue() {
		return _minPort;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer getMaxValue() {
		return _maxPort;
	}
}
