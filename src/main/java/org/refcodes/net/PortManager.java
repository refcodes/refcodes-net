/*
 * 
 */
package org.refcodes.net;

import java.net.ServerSocket;
import java.util.HashSet;
import java.util.Set;

/**
 * When implemented as singleton, then resources battling for dynamically
 * assigned ports my use this functionality to get and block a port until it is
 * freed again. The implementation must be thread safe! It is up to the skills
 * of the implementing programmer on how non-blocking the thread safe
 * implementation looks like!
 */
public class PortManager {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final Set<Integer> _ports = new HashSet<>();

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Determines whether the given Port-Number is already in use. This is
	 * achieved by testing whether another resource has reserved the port
	 * manually (via {@link #bindPort(int)}, {@link #bindAnyPort()} or
	 * {@link #bindNextPort(int)}) or if the port is already in use and cannot
	 * be bound, e.g. there is already some Server-Socket technically bound to
	 * the given port.
	 * 
	 * @param aPortNumber The port number to be tested if it is already bound.
	 * 
	 * @return True in case the port is being bound already, else false if it
	 *         still available.
	 */
	public synchronized boolean isPortBound( int aPortNumber ) {
		return _ports.contains( aPortNumber ) || !isPortAvaialble( aPortNumber );
	}

	/**
	 * Any free port in the range as defined by {@link PortRange#DYNAMIC_PORTS}
	 * is bound (reserved) and returned. A port is aught to be free when
	 * {@link #isPortBound(int)} returns false.
	 * 
	 * @return The free port being reserved. The {@link Integer} instance is the
	 *         handle for unbinding a port via {@link #unbindPort(Integer)}.
	 * 
	 * @throws PortNotFoundRuntimeException thrown in case no port can be
	 *         determined.
	 */
	public synchronized Integer bindAnyPort() {
		return bindNextPort( PortRange.DYNAMIC_PORTS );
	}

	/**
	 * The port is bound and returned. A port is aught to be free when
	 * {@link #isPortBound(int)} returns false.
	 *
	 * @param aPortNumber The Port-Number which to bind (reserve).
	 * 
	 * @return The port being reserved. The {@link Integer}instance is the
	 *         handle for unbinding a port via {@link #unbindPort(Integer)}.
	 * 
	 * @throws PortAlreadyBoundException the port already bound exception
	 */
	public synchronized Integer bindPort( int aPortNumber ) throws PortAlreadyBoundException {
		if ( isPortBound( aPortNumber ) ) {
			throw new PortAlreadyBoundException( "The port with number <" + aPortNumber + "> is already bound, please try later or another port.", aPortNumber );
		}
		_ports.add( aPortNumber );
		return aPortNumber;
	}

	/**
	 * Unbinds a port for the given Port-Number using the {@link Integer} as
	 * handle for unbinding. This means that another {@link Integer} instance
	 * with another identity fails to unbind the given port.
	 * 
	 * @param aPortNumber The Port-Number for the port to be unbound.
	 * 
	 * @return True in case the port has been unbound or false if the port was
	 *         not bound at all.
	 */
	public synchronized boolean unbindPort( Integer aPortNumber ) {
		return ( _ports.remove( aPortNumber ) );
	}

	/**
	 * The first free port in the range of the given begin Port-Number index and
	 * the given end Port-Number index is bound (reserved) and returned. A port
	 * is aught to be free when {@link #isPortBound(int)} returns false.
	 * 
	 * @param aBeginPortIndex The begin index (included) of the Port-Number
	 *        which to bind (reserve).
	 * @param aEndPortIndex The end index (included) of the Port-Number which to
	 *        bind (reserve).
	 * 
	 * @return The next free port being reserved. The {@link Integer} instance
	 *         is the handle for unbinding a port via
	 *         {@link #unbindPort(Integer)}.
	 * 
	 * @throws PortNotFoundRuntimeException thrown in case no port can be
	 *         determined.
	 */
	public synchronized Integer bindNextPort( int aBeginPortIndex, int aEndPortIndex ) {
		for ( int i = aBeginPortIndex; i < aEndPortIndex; i++ ) {
			if ( !isPortBound( i ) ) {
				_ports.add( i );
				return i;
			}
		}
		throw new PortNotFoundRuntimeException( "No port found " );
	}

	/**
	 * Tests whether there is already some Server-Socket technically bound to
	 * the given port. This method does not test if the port is reserved, it
	 * just tests if a Server-Socket could still technically bind to this port.
	 * 
	 * @param aPortNumber The Port-Number to be technically tested if a
	 *        Server-Socket could still bind to it.
	 * 
	 * @return True if the port could technically be bound to, else false if
	 *         some Server-Socket is already bound to this port (or the
	 *         Security-Manager prevents binding to the port).
	 */
	public boolean isPortAvaialble( int aPortNumber ) {
		try ( ServerSocket theSocket = new ServerSocket( aPortNumber ) ) {
			return true;
		}
		catch ( Exception e ) {
			_ports.add( aPortNumber );
			return false;
		}
	}

	/**
	 * The first free port in the range of the given Port-Number and the
	 * {@link PortRange#DYNAMIC_PORTS} max port number (as of
	 * {@link PortRange#getMaxValue()}) bound (reserved) and returned. A port is
	 * aught to be free when {@link #isPortBound(int)} returns false.
	 * 
	 * @param aBeginPortIndex The start index of the Port-Number which to bind
	 *        (reserve).
	 * 
	 * @return The next free port being reserved. The {@link Integer} instance
	 *         is the handle for unbinding a port via
	 *         {@link #unbindPort(Integer)}.
	 * 
	 * @throws PortNotFoundRuntimeException thrown in case no port can be
	 *         determined.
	 */
	public Integer bindNextPort( int aBeginPortIndex ) {
		return bindNextPort( aBeginPortIndex, PortRange.DYNAMIC_PORTS.getMaxValue() );
	}

	/**
	 * The first free port in the range of the given begin Port-Number index and
	 * the given end Port-Number index is bound (reserved) and returned. A port
	 * is aught to be free when {@link #isPortBound(int)} returns false.
	 *
	 * @param aPortRange The begin index (included) of the Port-Number which to
	 *        bind (reserve)and the end index (included) of the Port-Number
	 *        which to bind (reserve).
	 * 
	 * @return The next free port being reserved. The {@link Integer} instance
	 *         is the handle for unbinding a port via
	 *         {@link #unbindPort(Integer)}.
	 */
	public Integer bindNextPort( PortRange aPortRange ) {
		return bindNextPort( aPortRange.getMinValue(), aPortRange.getMaxValue() );
	}
}
