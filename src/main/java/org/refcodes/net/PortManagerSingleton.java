// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////
package org.refcodes.net;

/**
 * The singleton of the {@link PortManager} for system wide port management.
 */
public class PortManagerSingleton extends PortManager {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private static PortManagerSingleton _singleton = null;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new port manager singleton.
	 */
	protected PortManagerSingleton() {}

	/**
	 * Returns the singleton's instance as fabricated by this
	 * {@link PortManagerSingleton}.
	 * 
	 * @return The {@link PortManager} singleton's instance.
	 */
	public static PortManager getInstance() {
		if ( _singleton == null ) {
			synchronized ( PortManagerSingleton.class ) {
				if ( _singleton == null ) {
					_singleton = new PortManagerSingleton();
				}
			}
		}
		return _singleton;
	}
}
