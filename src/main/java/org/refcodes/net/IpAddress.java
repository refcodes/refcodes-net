// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.net;

import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import org.refcodes.data.Delimiter;
import org.refcodes.exception.UnhandledEnumBugException;
import org.refcodes.runtime.Host;

/**
 * Metrics of an IP-Address as of IPv4 or IPv6.
 */
public enum IpAddress {

	// /////////////////////////////////////////////////////////////////////////
	// ENUMS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * IPv4:
	 */
	IPV4(4, new int[] { 0, 0, 0, 0 }, new int[] { 127, 0, 0, 1 }, new int[] { 255, 255, 255, 255 }, "0.0.0.0", "127.0.0.1", "localhost", '.'),
	/**
	 * IPv6:
	 */
	IPV6(16, new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 }, null, "::", "::1", "localhost", ':');

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private int _length;
	private int[] _unspecified;
	private String _cidrUnspecified;
	private int[] _localhost;
	private String _cidrLocalhost;
	private String _localhostAlias;
	private char _cidrDelimiter;
	private int[] _broadcast;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private IpAddress( int aLength, int[] aUnspecified, int[] aLocalhost, int[] aBroadcast, String aCidrUnspecified, String aCidrLocalhost, String aLocalhostAlias, char aCidrDelimiter ) {
		_length = aLength;
		_unspecified = aUnspecified;
		_localhost = aLocalhost;
		_cidrUnspecified = aCidrUnspecified;
		_cidrLocalhost = aCidrLocalhost;
		_localhostAlias = aLocalhostAlias;
		_cidrDelimiter = aCidrDelimiter;
		_broadcast = aBroadcast;
	}

	/**
	 * Returns the length in bytes of the IP-Address.
	 * 
	 * @return The IP-Address' length in bytes.
	 */
	public int getLength() {
		return _length;
	}

	/**
	 * The byte representation of the "unspecified".
	 * 
	 * @return The byte representation of the "unspecified".
	 */
	public int[] getUnspecified() {
		return _unspecified.clone();
	}

	/**
	 * Returns the CIDR notation of the unspecified IP-Address. See also
	 * "https://en.wikipedia.org/wiki/Classless_Inter-Domain_Routing#CIDR_notation".
	 * 
	 * @return The CIDR notation for "unspecified".
	 */
	public String getCidrUnspecified() {
		return _cidrUnspecified;
	}

	/**
	 * The byte representation of the "localhost".
	 * 
	 * @return The byte representation of the "localhost".
	 */
	public int[] getLocalhost() {
		return _localhost.clone();
	}

	/**
	 * The byte representation of the "broadcast" address (if the protocol
	 * supports a broadcast).
	 * 
	 * @return The byte representation of the "broadcast" address or null if the
	 *         protocol does not support a broadcast address.
	 */
	public int[] getBroadcast() {
		return _broadcast.clone();

	}

	/**
	 * Returns the CIDR notation of the localhost IP-Address. See also
	 * "https://en.wikipedia.org/wiki/Classless_Inter-Domain_Routing#CIDR_notation".
	 * 
	 * @return The CIDR notation for "localhost".
	 */
	public String getCidrLocalhost() {
		return _cidrLocalhost;
	}

	/**
	 * The commonly used alias for the "localhost".
	 * 
	 * @return The "localhost" alias.
	 */
	public String getLocalhostAlias() {
		return _localhostAlias;
	}

	/**
	 * The delimiter used by the CIDR notation. See also
	 * "https://en.wikipedia.org/wiki/Classless_Inter-Domain_Routing#CIDR_notation".
	 * 
	 * @return The CIDR delimiter.
	 */
	public char getCidrDelimiter() {
		return _cidrDelimiter;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	public String toCidrNotation( int[] theIpAddress ) {

		if ( theIpAddress == null ) {
			return null;
		}
		if ( theIpAddress.length == 0 ) {
			return "";
		}

		if ( theIpAddress.length == _length ) {
			// Localhost |-->
			out: {
				for ( int i = 0; i < _localhost.length; i++ ) {
					if ( theIpAddress[i] != _localhost[i] ) {
						break out;
					}
				}
				return _cidrLocalhost;
			}
			// Localhost <--|
			// Unspecified|-->
			out: {
				for ( int i = 0; i < _unspecified.length; i++ ) {
					if ( theIpAddress[i] != _unspecified[i] ) {
						break out;
					}
				}
				return _cidrUnspecified;
			}
			// Unspecified <--|

			final StringBuilder theBuffer = new StringBuilder();
			String eValue;
			for ( int i = 0; i < _length; i++ ) {
				switch ( this ) {
				case IPV4 -> {
					eValue = Integer.toString( theIpAddress[i] );
					theBuffer.append( eValue );
				}
				case IPV6 -> {
					eValue = Integer.toHexString( theIpAddress[i] );
					if ( eValue.length() < 2 ) {
						eValue = "0" + eValue;
					}
					theBuffer.append( eValue );
				}
				default -> throw new UnhandledEnumBugException( this );
				}
				if ( i < _length - 1 && ( this != IPV6 || i % 2 != 0 ) ) {
					theBuffer.append( _cidrDelimiter );
				}
			}
			return theBuffer.toString();
		}
		final StringBuilder theBuffer = new StringBuilder();
		for ( int i = 0; i < theIpAddress.length - 1; i++ ) {
			theBuffer.append( Integer.toString( theIpAddress[i] ) );
			if ( i < theIpAddress.length - 1 ) {
				theBuffer.append( Delimiter.ARRAY.getChar() );
			}
		}
		throw new IllegalArgumentException( "The IP-Address <" + theBuffer.toString() + "> (in decimal) is neither of format IPv4 nor of format IPv6!" );
	}

	/**
	 * Creates an IP-Address as of an integer array for the current enumeration
	 * from the provided {@link String} in CIDR notation.
	 * 
	 * @param aCidrNotation The String in CIDR notation to be parsed.
	 * 
	 * @return The IP-Address as an array of integer values.
	 * 
	 * @throws IllegalArgumentException Thrown in case the provided
	 *         {@link String} is not of any known CIDR notation.
	 */
	public int[] fromCidrNotation( String aCidrNotation ) {
		if ( aCidrNotation == null ) {
			return null;
		}
		if ( aCidrNotation.isEmpty() ) {
			return _unspecified;
		}
		if ( _cidrLocalhost.equals( aCidrNotation ) || _localhostAlias.equals( aCidrNotation ) ) {
			return _localhost;
		}
		if ( _cidrUnspecified.equals( aCidrNotation ) ) {
			return _unspecified;
		}
		if ( aCidrNotation.indexOf( _cidrDelimiter ) != -1 ) {
			final StringTokenizer theTokenizer = new StringTokenizer( aCidrNotation, _cidrDelimiter + "" );
			int i = 0;
			final int[] theIpAddress = new int[_length];
			String eToken;
			while ( theTokenizer.hasMoreElements() && i < _length ) {
				eToken = theTokenizer.nextToken();
				switch ( this ) {
				case IPV4 -> {
					theIpAddress[i] = Integer.valueOf( eToken );
					if ( theIpAddress[i] > 255 || theIpAddress[i] < 0 ) {
						throw new IllegalArgumentException( "The value <" + theIpAddress[i] + "> at element index [" + i + "] (with a 2-digit hex value, e.g.'00' or 'FF' per element, beginning with index 0) for IP-Address <" + aCidrNotation + "> is out range!" );
					}
				}
				case IPV6 -> {
					if ( eToken.length() > 4 ) {
						throw new IllegalArgumentException( "The value tupel <" + eToken + "> beginning at elelment index [" + i + "] (with a 2-digit hex value, e.g.'00' or 'FF' per element, beginning with index 0) for IP-Address <" + aCidrNotation + "> has a wrong format!" );
					}
					while ( eToken.length() < 4 ) {
						eToken = "0" + eToken;
					}
					theIpAddress[i] = Integer.valueOf( eToken.substring( 0, 2 ), 16 );
					if ( theIpAddress[i] > 255 || theIpAddress[i] < 0 ) {
						throw new IllegalArgumentException( "The value <" + theIpAddress[i] + "> at element index [" + i + "] (with a 2-digit hex value, e.g.'00' or 'FF' per element, beginning with index 0) for IP-Address <" + aCidrNotation + "> is out range!" );
					}
					i++;
					theIpAddress[i] = Integer.valueOf( eToken.substring( 2, 4 ), 16 );
					if ( theIpAddress[i] > 255 || theIpAddress[i] < 0 ) {
						throw new IllegalArgumentException( "The value <" + theIpAddress[i] + "> at element index [" + i + "] (with 2-digit hex value, e.g.'00' or 'FF' per element, beginning with index 0) for IP-Address <" + aCidrNotation + "> is out range!" );
					}
				}
				default -> throw new UnhandledEnumBugException( this );
				}
				i++;
			}
			if ( i == _length ) {
				return theIpAddress;
			}
		}
		throw new IllegalArgumentException( "The String passed as IP-Address <" + aCidrNotation + "> is not an IP-Address!" );
	}

	/**
	 * Creates a {@link String} as of the CIDR notation from the provided
	 * IP-Address bytes. See also
	 * "https://en.wikipedia.org/wiki/Classless_Inter-Domain_Routing#CIDR_notation".
	 * 
	 * @param aIpAddress The IP-Address for which to generate a {@link String}
	 *        in CIDR notation.
	 * 
	 * @return The {@link String} in CIDR notation.
	 */
	public static String toString( int[] aIpAddress ) {
		if ( aIpAddress == null ) {
			return null;
		}
		if ( aIpAddress.length == 0 ) {
			return "";
		}
		for ( IpAddress eAddress : values() ) {
			if ( aIpAddress.length == eAddress.getLength() ) {
				return eAddress.toCidrNotation( aIpAddress );
			}
		}
		final StringBuilder theBuffer = new StringBuilder();
		for ( int i = 0; i < aIpAddress.length - 1; i++ ) {
			theBuffer.append( Integer.toString( aIpAddress[i] ) );
			if ( i < aIpAddress.length - 1 ) {
				theBuffer.append( Delimiter.ARRAY.getChar() );
			}
		}
		throw new IllegalArgumentException( "The IP-Address <" + theBuffer.toString() + "> (in decimal) is neither of format IPv4 nor of format IPv6!" );
	}

	/**
	 * Determines whether the given IP-Address represents the IP-Address of the
	 * "localhost" as of {@link #getLocalhost()}.
	 * 
	 * @param aIpAddress The IP-Address to be tested.
	 * 
	 * @return True in case we have an IP-Address representing the localhost,
	 *         else false.
	 */
	public static boolean isLocalhost( int[] aIpAddress ) {
		return Arrays.equals( IPV4.getLocalhost(), aIpAddress ) || Arrays.equals( IPV6.getLocalhost(), aIpAddress );
	}

	/**
	 * Determines whether the given IP-Address is in the scope of the the
	 * loopback interfaces.
	 * 
	 * @param aIpAddress The IP-Address to be tested.
	 * 
	 * @return True in case we have an IP-Address belonging to the loopback
	 *         IP-Address block, else false.
	 */
	public static boolean isLoopback( int[] aIpAddress ) {
		if ( aIpAddress.length == IPV4.getLength() ) {
			if ( aIpAddress[0] == IPV4.getLocalhost()[0] ) {
				return true;
			}
		}
		else if ( aIpAddress.length == IPV6.getLength() ) {
			for ( int i = 0; i < IPV6.getLocalhost().length - 1 /* -1 !! */; i++ ) {
				if ( aIpAddress[i] != IPV6.getLocalhost()[i] ) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	/**
	 * Determines whether the given IP-Address is the broadcast address.
	 * 
	 * @param aIpAddress The IP-Address to be tested.
	 * 
	 * @return True in case we have an IP-Address being the broadcast address,
	 *         else false.
	 */
	public static boolean isBroadcast( int[] aIpAddress ) {
		if ( aIpAddress.length == IPV4.getLength() ) {
			for ( int i = 0; i < IPV4.getBroadcast().length; i++ ) {
				if ( IPV4.getBroadcast()[i] != aIpAddress[i] ) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	/**
	 * Creates an IP-Address as of an integer array for the first of the
	 * enumeration elements being able to successfully parse the provided
	 * {@link String} in CIDR notation.
	 * 
	 * @param aCidrNotation The String in CIDR notation to be parsed.
	 * 
	 * @return The IP-Address as an array of integer values.
	 * 
	 * @throws IllegalArgumentException Thrown in case the provided
	 *         {@link String} is not of any known CIDR notation.
	 */
	public static int[] fromAnyCidrNotation( String aCidrNotation ) {
		if ( aCidrNotation == null ) {
			return null;
		}
		for ( IpAddress eAddress : values() ) {
			if ( aCidrNotation.indexOf( eAddress.getCidrDelimiter() ) != -1 ) {
				return eAddress.fromCidrNotation( aCidrNotation );
			}
		}
		throw new IllegalArgumentException( "The IP-Address <" + aCidrNotation + "> is neither of format IPv4 nor of format IPv6!" );
	}

	/**
	 * Tries to determine a no-localhost IP-Address for this machine. The best
	 * guess is returned. If none no-localhost address is found, then the
	 * localhost's IP-Address may be returned (as of
	 * {@link IpAddress#getLocalhost()}).
	 * 
	 * @return The best guest for a no-localhost IP-Address or as a fall back
	 *         the localhost's IP-Address (as of
	 *         {@link IpAddress#getLocalhost()}) may be returned.
	 * 
	 * @throws IOException Thrown to indicate that the IP address of a host
	 *         could not be determined.
	 */
	public static int[] toHostIpAddress() throws IOException {
		int[] eIpAddress;
		final Enumeration<NetworkInterface> eNetworks = NetworkInterface.getNetworkInterfaces();
		NetworkInterface eNetwork;
		Enumeration<InetAddress> eAddresses;
		while ( eNetworks.hasMoreElements() ) {
			eNetwork = eNetworks.nextElement();
			if ( eNetwork.getDisplayName() == null || !eNetwork.getDisplayName().toLowerCase().contains( "Loopback" ) ) {
				eAddresses = eNetwork.getInetAddresses();
				while ( eAddresses.hasMoreElements() ) {
					try {
						final InetAddress nextElement = eAddresses.nextElement();
						eIpAddress = IpAddress.fromAnyCidrNotation( nextElement.getHostAddress() );
						if ( !IpAddress.isLoopback( eIpAddress ) ) {
							return eIpAddress;
						}
					}
					catch ( IllegalArgumentException ignore ) {
						/* ignore */
					}
				}
			}
		}
		return IpAddress.fromAnyCidrNotation( InetAddress.getLocalHost().getHostAddress() );
	}

	/**
	 * Determines whether the given {@link String} represents a valid
	 * IP-Address.
	 * 
	 * @param aCidrNotation The Text in CIDR notation to be tested.
	 * 
	 * @return True in case of being an IP-Address, else false.
	 */
	public static boolean isAnyCidrNotation( String aCidrNotation ) {
		try {
			return ( fromAnyCidrNotation( aCidrNotation ) != null );
		}
		catch ( IllegalArgumentException irgnore ) {
			return false;
		}
	}

	/**
	 * Determines whether the given {@link String} represents a valid CIDR
	 * notation.
	 * 
	 * @param aCidrNotation The Text in CIDR notation to be tested.
	 * 
	 * @return True in case of being an IP-Address, else false.
	 */
	public boolean isCidrNotation( String aCidrNotation ) {
		try {
			return ( fromCidrNotation( aCidrNotation ) != null );
		}
		catch ( IllegalArgumentException irgnore ) {
			return false;
		}
	}

	/**
	 * Tries to determine the known IP-Addresses from the accessible network
	 * segment. It tries to invoke "arp -a" and also tries to manually detect
	 * the visible IP-Addresses.
	 * 
	 * @return A set containing the visible IP-Addresses in CIDR notation.
	 */
	public static Set<String> toVisibleIpAddresses() {
		final Set<String> theCidrNotations = new HashSet<>();

		final List<String> theExecs = new ArrayList<>();
		theExecs.add( "arp -a" );
		try {
			final int[] theIpAddress = toHostIpAddress();
			String theSubnetMask = "";
			if ( theIpAddress.length == IPV4.getLength() ) {
				for ( int i = 0; i < theIpAddress.length - 1; i++ ) {
					theSubnetMask += theIpAddress[i];
					if ( i < theIpAddress.length - 2 ) {
						theSubnetMask += IPV4.getCidrDelimiter();
					}
				}
			}
			theExecs.add( "nmap -sn -oG ip.txt " + theSubnetMask + ".1-255" );
		}
		catch ( IOException ignore ) {
			/* ignore */
		}

		try {
			for ( String eExec : theExecs ) {
				final String theResult = Host.exec( eExec );
				final String[] theElements = theResult.split( " +" );
				for ( String eElement : theElements ) {
					try {
						final int[] eIpAddress = IpAddress.fromAnyCidrNotation( eElement );
						if ( eIpAddress != null && !IpAddress.isLocalhost( eIpAddress ) && !IpAddress.isBroadcast( eIpAddress ) && !eElement.equals( IpAddress.toString( IpAddress.toHostIpAddress() ) ) ) {
							theCidrNotations.add( eElement );
						}
					}
					catch ( IllegalArgumentException ignore ) {
						/* ignore */
					}
				}
			}
		}
		catch ( IOException ignore ) {
			/* ignore */
		}

		// try {
		// 		if ( theCidrNotations.isEmpty() ) {
		//			int[] theUnsigned = toHostIpAddress();
		//			if ( theUnsigned != null && theUnsigned.length == IPV4.getLength() ) {
		//				byte[] theSigned = new byte[theUnsigned.length];
		//				for ( int i = 0; i < theSigned.length; i++ ) {
		//					theSigned[i] = (byte) theUnsigned[i];
		//				}
		//				InetAddress eAddress;
		//				for ( int i = 1; i <= 254; i++ ) {
		//					theSigned[IPV4.getLength() - 1] = (byte) i;
		//					eAddress = InetAddress.getByAddress( theSigned );
		//					if ( eAddress.isReachable( 10 ) ) {
		//						theCidrNotations.add( eAddress.getHostAddress() );
		//					}
		//					else if ( !eAddress.getHostAddress().equals( eAddress.getHostName() ) ) {
		//						theCidrNotations.add( eAddress.getHostAddress() );
		//					}
		//				}
		//			}
		// 		}
		//	}
		//	catch ( IOException ignore ) {
		//		/* ignore */
		//	}

		return theCidrNotations;
	}
}
