// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.net;

import org.refcodes.net.NetRuntimeException.PortRuntimeException;

/**
 * Thrown in case the given port {@link #getPort()} is already in use.
 */
public class PortAlreadyBoundRuntimeException extends PortRuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * {@inheritDoc}
	 */
	public PortAlreadyBoundRuntimeException( String aMessage, int aPort, String aErrorCode ) {
		super( aMessage, aPort, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public PortAlreadyBoundRuntimeException( String aMessage, int aPort, Throwable aCause, String aErrorCode ) {
		super( aMessage, aPort, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public PortAlreadyBoundRuntimeException( String aMessage, int aPort, Throwable aCause ) {
		super( aMessage, aPort, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public PortAlreadyBoundRuntimeException( String aMessage, int aPort ) {
		super( aMessage, aPort );
	}

	/**
	 * {@inheritDoc}
	 */
	public PortAlreadyBoundRuntimeException( int aPort, Throwable aCause, String aErrorCode ) {
		super( aPort, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public PortAlreadyBoundRuntimeException( int aPort, Throwable aCause ) {
		super( aPort, aCause );
	}
}
