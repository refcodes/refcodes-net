// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.net;

import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import java.util.Arrays;
import java.util.Set;
import org.junit.jupiter.api.Test;
import org.refcodes.exception.Trap;
import org.refcodes.net.IpAddressAccessor.IpAddressProperty;
import org.refcodes.runtime.SystemProperty;

public class IpAddressTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testHostAddress() throws IOException {
		System.out.println( Arrays.toString( org.refcodes.net.IpAddress.toHostIpAddress() ) );
	}

	@Test
	public void testIpV4AddressOk() {
		final String fromCidrNotation = "127.0.0.1";
		final IpAddress theIpAddress = new IpAddress();
		theIpAddress.fromCidrNotation( fromCidrNotation );
		assertEquals( theIpAddress.getIpAddress()[0], 127 );
		assertEquals( theIpAddress.getIpAddress()[1], 0 );
		assertEquals( theIpAddress.getIpAddress()[2], 0 );
		assertEquals( theIpAddress.getIpAddress()[3], 1 );
		final String toCidrNotation = theIpAddress.toCidrNotation();
		// if (IS_LOG_TESTS_ENABLED) System.out.println( fromCidrNotation + " --> " + toCidrNotation );
		assertEquals( fromCidrNotation, toCidrNotation );
	}

	@Test
	public void testIpV4AddressBad() {
		try {
			final String fromCidrNotation = "127.-1.0.1";
			final IpAddress theIpAddress = new IpAddress();
			theIpAddress.fromCidrNotation( fromCidrNotation );
			fail( "Expecting an <IllegalStateException> exception!" );
		}
		catch ( IllegalArgumentException e ) {/* Expected */ }
		try {
			final String fromCidrNotation = "256.0.0.1";
			final IpAddress theIpAddress = new IpAddress();
			theIpAddress.fromCidrNotation( fromCidrNotation );
			fail( "Expecting an <IllegalStateException> exception!" );
		}
		catch ( IllegalArgumentException e ) {/* Expected */ }
		try {
			final String fromCidrNotation = "127.0.0.1024";
			final IpAddress theIpAddress = new IpAddress();
			theIpAddress.fromCidrNotation( fromCidrNotation );
			fail( "Expecting an <IllegalStateException> exception!" );
		}
		catch ( IllegalArgumentException e ) {/* Expected */ }
	}

	@Test
	public void testIpV6AddressOk1() {
		final String fromCidrNotation = "::1";
		final IpAddress theIpAddress = new IpAddress();
		theIpAddress.fromCidrNotation( fromCidrNotation );
		assertEquals( theIpAddress.getIpAddress()[0], 0 );
		assertEquals( theIpAddress.getIpAddress()[1], 0 );
		assertEquals( theIpAddress.getIpAddress()[2], 0 );
		assertEquals( theIpAddress.getIpAddress()[3], 0 );
		assertEquals( theIpAddress.getIpAddress()[4], 0 );
		assertEquals( theIpAddress.getIpAddress()[5], 0 );
		assertEquals( theIpAddress.getIpAddress()[6], 0 );
		assertEquals( theIpAddress.getIpAddress()[7], 0 );
		assertEquals( theIpAddress.getIpAddress()[8], 0 );
		assertEquals( theIpAddress.getIpAddress()[9], 0 );
		assertEquals( theIpAddress.getIpAddress()[10], 0 );
		assertEquals( theIpAddress.getIpAddress()[11], 0 );
		assertEquals( theIpAddress.getIpAddress()[12], 0 );
		assertEquals( theIpAddress.getIpAddress()[13], 0 );
		assertEquals( theIpAddress.getIpAddress()[14], 0 );
		assertEquals( theIpAddress.getIpAddress()[15], 1 );
		final String toCidrNotation = theIpAddress.toCidrNotation();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( fromCidrNotation + " --> " + toCidrNotation );
		}
		assertEquals( fromCidrNotation, toCidrNotation );
	}

	@Test
	public void testIpV6AddressOk2() {
		final String fromCidrNotation = "2607:f0d0:1002:0051:0000:0000:0000:0004";
		final IpAddress theIpAddress = new IpAddress();
		theIpAddress.fromCidrNotation( fromCidrNotation );
		assertEquals( theIpAddress.getIpAddress()[0], 0x26 );
		assertEquals( theIpAddress.getIpAddress()[1], 0x07 );
		assertEquals( theIpAddress.getIpAddress()[2], 0xf0 );
		assertEquals( theIpAddress.getIpAddress()[3], 0xd0 );
		assertEquals( theIpAddress.getIpAddress()[4], 0x10 );
		assertEquals( theIpAddress.getIpAddress()[5], 0x02 );
		assertEquals( theIpAddress.getIpAddress()[6], 0x00 );
		assertEquals( theIpAddress.getIpAddress()[7], 0x51 );
		assertEquals( theIpAddress.getIpAddress()[8], 0x00 );
		assertEquals( theIpAddress.getIpAddress()[9], 0x00 );
		assertEquals( theIpAddress.getIpAddress()[10], 0x00 );
		assertEquals( theIpAddress.getIpAddress()[11], 0x00 );
		assertEquals( theIpAddress.getIpAddress()[12], 0x00 );
		assertEquals( theIpAddress.getIpAddress()[13], 0x00 );
		assertEquals( theIpAddress.getIpAddress()[14], 0x00 );
		assertEquals( theIpAddress.getIpAddress()[15], 0x04 );
		final String toCidrNotation = theIpAddress.toCidrNotation();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( fromCidrNotation + " --> " + toCidrNotation );
		}
		assertEquals( fromCidrNotation, toCidrNotation );
	}

	@Test
	public void testIpV6AddressBad() {
		try {
			final String fromCidrNotation = "2607:f0d0:1002:0051:0000:0000:0000:-1004";
			final IpAddress theIpAddress = new IpAddress();
			theIpAddress.fromCidrNotation( fromCidrNotation );
			fail( "Expecting an <IllegalStateException> exception!" );
		}
		catch ( IllegalArgumentException e ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( Trap.asMessage( e ) );
			}
		}
		try {
			final String fromCidrNotation = "2607:f0d0:1002:0051:0000:0000:0000";
			final IpAddress theIpAddress = new IpAddress();
			theIpAddress.fromCidrNotation( fromCidrNotation );
			fail( "Expecting an <IllegalStateException> exception!" );
		}
		catch ( IllegalArgumentException e ) {/* Expected */ }
		try {
			final String fromCidrNotation = "127.0.0.1024";
			final IpAddress theIpAddress = new IpAddress();
			theIpAddress.fromCidrNotation( fromCidrNotation );
			fail( "Expecting an <IllegalStateException> exception!" );
		}
		catch ( IllegalArgumentException e ) {/* Expected */ }
	}

	@Test
	public void testVisibleIpAddresses() {
		final Set<String> theIpAddresses = org.refcodes.net.IpAddress.toVisibleIpAddresses();
		for ( String eIp : theIpAddresses ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eIp );
			}
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static class IpAddress implements IpAddressProperty {
		private int[] _ipAddress = null;

		@Override
		public int[] getIpAddress() {
			return _ipAddress;
		}

		@Override
		public void setIpAddress( int[] aIpAddress ) {
			_ipAddress = aIpAddress;
		}
	}
}
