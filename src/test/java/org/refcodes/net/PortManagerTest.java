// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.net;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;

public class PortManagerTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testBindPort() {
		final Integer thePort = PortManagerSingleton.getInstance().bindAnyPort();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Determined port <" + thePort + "> ..." );
		}
		assertTrue( PortManagerSingleton.getInstance().isPortBound( thePort ) );
		assertTrue( PortManagerSingleton.getInstance().isPortAvaialble( thePort ) );
		boolean isUnbound = PortManagerSingleton.getInstance().unbindPort( thePort );
		assertTrue( isUnbound );
		isUnbound = PortManagerSingleton.getInstance().unbindPort( thePort );
		assertFalse( isUnbound );
	}

	@Test
	public void testUnbindPort() {
		final Integer thePort = PortManagerSingleton.getInstance().bindAnyPort();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Determined port <" + thePort + "> ..." );
		}
		assertTrue( PortManagerSingleton.getInstance().isPortBound( thePort ) );
		assertTrue( PortManagerSingleton.getInstance().isPortAvaialble( thePort ) );
		try {
			PortManagerSingleton.getInstance().unbindPort( Integer.valueOf( thePort.intValue() ) );
		}
		catch ( IllegalArgumentException ignore ) { /* expected */ }
	}

	@Test
	public void testPortBound() {
		final Integer thePort = PortManagerSingleton.getInstance().bindAnyPort();
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Determined port <" + thePort + "> ..." );
		}
		try {
			PortManagerSingleton.getInstance().bindPort( thePort );
			fail( "Expecting port <" + thePort + "> to be already bound!" );
		}
		catch ( PortAlreadyBoundException ignore ) { /* expected */ }
	}
}
